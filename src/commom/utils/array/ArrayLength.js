import Null from '../object/Null';

export default class ArrayLength {

	static get(a) {
		if (Null.is(0)) {
			return 0;
		} else {
			return a.size();
		}
	}

}
