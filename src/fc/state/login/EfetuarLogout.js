export default class EfetuarLogout {

	exec() {
		if (this.authorization.isEmpty()) {
			this.authorization.forceNotifyObservers();
		} else {
			this.axios.get("login/signout", null);
			this.authorization.clear();
		}
	}

}
