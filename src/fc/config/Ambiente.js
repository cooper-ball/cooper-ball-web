export default class Ambiente {

	static config;

	static set(value) {
		Ambiente.config = value;
	}

	static getLocalStorage() {
		return Ambiente.config.getLocalStorage();
	}

	static getSessionStorage() {
		return Ambiente.config.getSessionStorage();
	}

	static getButton(title, onClick) {
		return Ambiente.config.getButton(title, onClick);
	}

	static showMessageInfo(s) {
		Ambiente.config.showMessageInfo(s);
	}

	static showMessageError(s) {
		Ambiente.config.showMessageError(s);
	}

}
