import ArrayLst from '../../commom/utils/array/ArrayLst';
import axios from 'axios';
import Null from '../../commom/utils/object/Null';
import PromiseBuilder from '../../commom/utils/comum/PromiseBuilder';
import Requisicao from './Requisicao';
import StringCompare from '../../commom/utils/string/StringCompare';
import StringParse from '../../commom/utils/string/StringParse';
import StringRight from '../../commom/utils/string/StringRight';

export default class FcAxios {

	requisicoes = new ArrayLst();

	init() {
		this.uriBase = this.variaveisDeAmbiente.getUriBase();
		if (this.uriBase.endsWith("/")) {
			this.uriBase = StringRight.ignore1(this.uriBase);
		}
	}

	getHeaders() {

		let o = {};
		o["content-type"] = "application/json;charset=UTF-8";
		o.accept = "*/*";

		if (this.logado.isTrue()) {
			o.authorization = this.authorization.get();
		}

		return o;
	}

	getConfig() {
		let config = {};
		config.headers = this.getHeaders();
		config.withCredentials = true;
		return config;
	}

	exec(key, uri, runner, callback) {

		if (this.logado.isTrue()) {

			let o = this.cache.get(key);

			if (!Null.is(o)) {
				return PromiseBuilder.ft(() => {
					if (!Null.is(callback)) {
						callback(o);
					}
					return o;
				});
			}

		}

		let requisicaoEmAndamento = this.requisicoes.unique(o => StringCompare.eq(o.key, key));

		if (!Null.is(requisicaoEmAndamento)) {
			if (!Null.is(callback)) {
				requisicaoEmAndamento.callbacks.add(callback);
			}
			return requisicaoEmAndamento.promessa;
		}

		let req = new Requisicao();
		req.key = key;

		let config = this.getConfig();
		req.promessa = runner(this.uriBase + uri, config)
		.then(res => {
			this.cache.set(key, res);
			req.callbacks.forEach(o => o(res));
			return res;
		}).finally(() => {
			this.requisicoes.removeObject(req);
		});

		if (!Null.is(callback)) {
			req.callbacks.add(callback);
		}

		this.requisicoes.add(req);

		return req.promessa;

	}

	get(uri, callback) {
		uri = this.tratarUrl(uri);
		let key = "get+"+uri;
		return this.exec(key, uri, (ur, config) => axios.get(ur, config), callback);
	}

	delete(uri, callback) {
		uri = this.tratarUrl(uri);
		let key = "del+"+uri;
		return this.exec(key, uri, (ur, config) => axios.delete(ur, config), callback);
	}

	post(uri, params, callback) {
		uri = this.tratarUrl(uri);
		let key = "post+" + uri + "+" + StringParse.get(params);
		return this.exec(key, uri, (ur, config) => axios.post(ur, params, config), callback);
	}

	tratarUrl(uri) {

		if (!uri.startsWith("/")) {
			uri = "/" + uri;
		}

		return uri;

	}
}
