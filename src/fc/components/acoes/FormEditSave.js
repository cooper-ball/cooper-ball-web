import Null from '../../../commom/utils/object/Null';
import {message} from 'antd';

export default class FormEditSave {

	saveJaFoiClicado = false;

	constructor(form) {
		this.form = form;
	}

	run() {

		if (this.form.getCampos().houveAlteracoes.isFalse()) {
			return;
		}

		let salvou = this.form.getCampos().save(this.form.getVinculo(), () => {
			if (this.form.isVinculado()) {
				message.info("As alterações terão efeito quando o registro for salvo!");
			} else {
				message.success("Registro Salvo com Sucesso!");
				this.form.afterSave();
			}
		});

		if (salvou) {
			this.saveJaFoiClicado = false;

			if (!Null.is(this.onConfirm)) {
				this.onConfirm();
			} else if (this.form.isVinculado()) {
				this.form.close();
			}

		} else {
			this.saveJaFoiClicado = true;
			this.form.forceUpdate();
			message.error("Não foi possível salvar o registro pois há impedimentos!");
		}

	}

}
