import React from 'react';
import AlignItens from '../../app/misc/consts/enums/AlignItens';
import CommonStyles from '../../app/misc/styles/CommonStyles';
import FormEditButtons from './FormEditButtons';
import FormEditDelete from './acoes/FormEditDelete';
import FormEditSave from './acoes/FormEditSave';
import FormGenerico from './FormGenerico';
import FormItemInput from '../../antd/form/FormItemInput';
import Frag from '../../react/Frag';
import KeyDownObservers from '../../web/misc/KeyDownObservers';
import Null from '../../commom/utils/object/Null';
import PopoverDate from './PopoverDate';
import Style from '../../app/misc/utils/Style';
import Tb from '../../web/Tb';
import Td from '../../web/Td';
import TextAlign from '../../app/misc/consts/enums/TextAlign';
import Tr from '../../web/Tr';
import {EditOutlined} from '@ant-design/icons';

export default class FormEdit extends FormGenerico {
	constructor(props){
		super(props);
		this.state.abaSelecionada = "Geral";
	}

	acaoSave = new FormEditSave(this);
	acaoDelete = new FormEditDelete(this);

	init(entidadeP) {
		this.entidade = entidadeP;
		KeyDownObservers.removeByClass(this);
		this.init2();
	}

	init2() {}

	getFooter() {
		return (
			<Tb>
				<Tr>
					<Td style={FormEdit.TD_BUTTONS_LEFT}>
						{this.getBotoesCustomizados()}
					</Td>
					<Td style={FormEdit.TD_BUTTONS_RIGHT}>
						<FormEditButtons
							campos={this.getCampos()}
							onCancel={() => this.esc()}
							onSave={() => this.acaoSave.run()}
							onDelete={this.getOnDelete()}
							setAbaSelecionada={s => this.setAbaSelecionada(s)}
							saveJaFoiClicado={this.acaoSave.saveJaFoiClicado}
							vinculado={this.isVinculado()}
							somenteUpdate={this.props.somenteUpdate}
							abaSelecionada={this.state.abaSelecionada}
						/>
					</Td>
				</Tr>
			</Tb>
		);
	}

	getTop() {
		return (
			<Tb>
				<Tr>
					<Td style={FormEdit.TD_BUTTONS_LEFT}>
						{super.getTop()}
					</Td>
					{this.botoesSuperiores()}
				</Tr>
			</Tb>
		);
	}

	botoesSuperiores() {
		return null;
	}

	getOnDelete() {
		if (!this.podeExcluir()) return null;
		return () => this.delete();
	}

	delete() {
		this.acaoDelete.run();
	}

	podeExcluir() {

		if (this.props.somenteUpdate) {
			return false;
		}

		if (!this.getPermissoes().podeExcluir()) {
			return false;
		}

		if (this.getCampos().registroBloqueado.isTrue()) {
			return false;
		}

		return true;

	}

	getBotoesCustomizados() {
		return null;
	}

	didMount2() {
		this.acaoSave.onConfirm = this.props.onConfirm;
		this.acaoDelete.onDelete = this.props.onDelete;
		this.observar(this.getCampos().controleVinculado);
		this.observar(this.getCampos().modalComentarios);
	}

	afterSave() {}

	getBody() {
		if (this.esteFormEstahExibindoAlgumModal()) {
			return (
				<Frag>
					{this.getTabs()}
					{this.getModal()}
				</Frag>
			);
		} else {
			return this.getTabs();
		}
	}

	close() {
		if (this.getCampos().controleVinculado.isFalse()) {
			if (this.isVinculado()) {
				this.props.vinculo.set(false);
			}
			if (!Null.is(this.props.onClose)) {
				this.props.onClose();
			}
		}
	}

	cancelar() {
		if (!this.getCampos().isReadOnly() && this.getCampos().houveAlteracoes.isTrue()) {
			this.getCampos().cancelarAlteracoes();
		} else {
			this.close();
		}
	}

	confirmarImpl() {
		if (this.esteFormEstahExibindoAlgumModal()) {
			return;
		}
		this.acaoSave.run();
	}

	esteFormEstahExibindoAlgumModal() {
		return this.getCampos().controleVinculado.isTrue() || this.getCampos().modalComentarios.isTrue();
	}

	inputVinculado(bind, lg) {
		return (
			<FormItemInput
				after={this.getIconVinculado(bind)}
				bind={bind}
				lg={lg}
			/>
		);
	}

	getIconVinculado(bind) {
		if (bind.isDisabled() && bind.isEmpty()) {
			return null;
		}
		return (
			<EditOutlined
				style={FormEdit.STYLE_ICON.get()}
				onClick={() => bind.change()}
			/>
		);
	}

	getModal() {
		throw new Error("???");
	}

	isVinculado() {
		return !Null.is(this.props.vinculo);
	}

	getVinculo() {
		return this.props.vinculo;
	}

	inputData(bind, lg) {
		return <FormItemInput after={<PopoverDate bind={bind}/>} bind={bind} lg={lg}/>;
	}

	getTitle() {

		let cps = this.getCampos();

		if (Null.is(cps)) {
			throw new Error("campos is null");
		}

		if (cps.id.isEmpty()) {
			cps.setNovo();
		}

		let s = cps.id.get() < 0 ? " - Novo" : " - " + cps.id.asString();
		return this.getTitleImpl() + s;

	}

	ctrlDel() {
		this.delete();
	}
	setWidthForm = o => this.setState({widthForm:o});
	setAbaSelecionada = o => this.setState({abaSelecionada:o});
}
FormEdit.STYLE_ICON = CommonStyles.POINTER;
FormEdit.TD_BUTTONS_LEFT = Style.create().alignItems(AlignItens.flexStart).textAlign(TextAlign.left).paddingTop(10).paddingBottom(10).paddingLeft(20);
FormEdit.TD_BUTTONS_RIGHT = Style.create().alignItems(AlignItens.flexEnd).textAlign(TextAlign.right).paddingTop(10).paddingBottom(10).paddingRight(20);

FormEdit.defaultProps = {
	...FormGenerico.defaultProps,
	somenteUpdate: false
}
