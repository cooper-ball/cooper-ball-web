import React from 'react';
import Frag from '../../react/Frag';
import LayoutApp from './LayoutApp';
import SuperComponent from '../../app/misc/components/SuperComponent';
import {Card} from 'antd';
import {CaretDownOutlined} from '@ant-design/icons';
import {CaretUpOutlined} from '@ant-design/icons';

export default class GroupCard extends SuperComponent {
	constructor(props){
		super(props);
		this.state.expandido = true;
	}

	render0() {
		return (
			<Card
				title={this.props.title}
				size={"small"}
				style={GroupCard.TITULO_GROUP.get()}
				className={"card-table"}
				extra={this.getExtra()}>
				{this.getChildren()}
			</Card>
		);
	}

	getExtra() {

		if (this.state.expandido) {
			return (
				<Frag>
					{this.props.extra}
					<CaretUpOutlined
					onClick={() => this.setExpandido(false)}
					style={this.newStyle().marginLeft(10).get()}
					/>
				</Frag>
			);
		} else {
			return (
				<Frag>
					{this.props.extra}
					<CaretDownOutlined
					onClick={() => this.setExpandido(true)}
					style={this.newStyle().marginLeft(10).get()}
					/>
				</Frag>
			);
		}

	}

	getChildren() {
		if (this.state.expandido) {
			return this.props.children;
		} else {
			return null;
		}
	}

	didMount() {
		if (this.props.reprimido) {
			this.setExpandido(false);
		}
	}
	setExpandido = o => this.setState({expandido:o});
}
GroupCard.TITULO_GROUP = LayoutApp.createStyle().marginTop(15);

GroupCard.defaultProps = {
	...SuperComponent.defaultProps,
	reprimido: false
}
