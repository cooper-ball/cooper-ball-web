import React from 'react';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import BotaoType from '../../../antd/BotaoType';
import Color from '../../../app/misc/consts/enums/Color';
import Coluna from '../tabela/Coluna';
import Div from '../../../web/Div';
import FcBotao from '../FcBotao';
import FormEditButtons from '../FormEditButtons';
import FormGenerico from '../FormGenerico';
import Frag from '../../../react/Frag';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import LayoutApp from '../LayoutApp';
import Loading from '../../../antd/Loading';
import ModalCamposAlterados from '../campoAlterado/ModalCamposAlterados';
import Span from '../../../web/Span';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import Tabela from '../tabela/Tabela';
import TextAlign from '../../../app/misc/consts/enums/TextAlign';

export default class ModalAuditoria extends FormGenerico {

	rd() {
		if (this.props.auditoria.list.carregado()) {
			return this.render1();
		} else if (!StringEmpty.is(this.props.auditoria.list.getMensagemErro())) {
			let s = "Ocorreu um erro no servico: " + this.props.auditoria.list.getMensagemErro();
			return <Span style={ModalAuditoria.STYLE_ERROR} text={s}/>;
		} else {
			setTimeout(() => this.props.auditoria.list.carregar(), 250);
			return <Loading/>;
		}
	}

	closeModalCamposAlterados() {
		this.props.auditoria.list.set(false);
	}

	render1() {
		return (
			<Tabela
			bind={this.props.auditoria.list}
			colunas={ModalAuditoria.COLUNAS}
			onClick={o => this.edit(o)}
			funcGetEditIcon={o => {
				if (IntegerCompare.eq(o.idTipo, 2)) {
					return Tabela.ICONE_SEARCH;
				} else {
					return null;
				}
			}}
			/>
		);
	}

	edit(o) {
		if (IntegerCompare.eq(o.idTipo, 2)) {
			this.props.auditoria.detalharAuditoria(o);
		}
	}

	getTitle() {
		return "Auditoria";
	}

	getBody() {
		return (
			<Frag>
				{this.rd()}
				{this.getModal()}
			</Frag>
		);
	}

	getModal() {
		if (this.props.auditoria.list.isTrue()) {
			return <ModalCamposAlterados valores={this.props.auditoria.auditoriaItem.alteracoes} onClose={() => this.closeModalCamposAlterados()} campos={this.props.auditoria.campos}/>;
		} else {
			return null;
		}
	}

	getFooter() {
		return (
			<Div style={ModalAuditoria.STYLE_FOOTER}>
				<FcBotao style={FormEditButtons.STYLE_BUTTON} type={BotaoType.normal} acao={() => this.close()} title={"Sair (Esc)"}/>
			</Div>
		);
	}

	ehModal() {
		return true;
	}

	close() {
		this.props.auditoria.modalAuditoria.set(false);
	}

	didMount2() {
		this.observar(this.props.auditoria.list);
	}
	setWidthForm = o => this.setState({widthForm:o});
}
ModalAuditoria.STYLE_FOOTER = LayoutApp.createStyle().w100().padding(10).paddingRight(20);
ModalAuditoria.TIPO = new Coluna(15, null, "Ação", o => o.tipo, TextAlign.center);
ModalAuditoria.USUARIO = new Coluna(34, null, "Usuário", o => o.usuario, TextAlign.center);
ModalAuditoria.DATA = new Coluna(15, null, "Data/Hora", o => o.data, TextAlign.center);
ModalAuditoria.TEMPO = new Coluna(15, null, "Tempo", o => o.tempo, TextAlign.center);
ModalAuditoria.COLUNAS = ArrayLst.build(ModalAuditoria.TIPO, ModalAuditoria.USUARIO, ModalAuditoria.DATA, ModalAuditoria.TEMPO);
ModalAuditoria.STYLE_ERROR = LayoutApp.createStyle().margin(10).bold(true).color(Color.red);

ModalAuditoria.defaultProps = FormGenerico.defaultProps;
