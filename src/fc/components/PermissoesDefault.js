import Permissoes from './Permissoes';

export default class PermissoesDefault extends Permissoes {

	afterConstruct() {
		this.set(false);
	}

	podeVer() {
		return true;
	}

	podeIncluir() {
		return false;
	}

	podeAlterar() {
		return false;
	}

	podeExcluir() {
		return false;
	}

}
