import Box from '../../commom/utils/comum/Box';
import VServiceList from './VServiceList';

export default class VConsultaList extends VServiceList {

	axiosBox = new Box();

	constructor(titleP,mergeFunctionP, carragarCallBackP, uri, getParamsP, a) {
		super(titleP, mergeFunctionP, carragarCallBackP, uri);
		this.getParams = getParamsP;
		this.axiosBox.set(a);
	}

	getParametros() {
		return this.getParams();
	}

	preparadoParaBusca() {
		return true;
	}

	getAxios() {
		return this.axiosBox.get();
	}

}
