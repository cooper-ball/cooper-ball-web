import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VCpf from '../../../app/campos/support/VCpf';

export default class CampoConsultaCpf extends CampoConsulta {

	a = new VCpf();

	constructor(nomeCampoP, titulo, notNull) {
		super(nomeCampoP, titulo, CampoConsultaCpf.OPERADORES_POSSIVEIS, notNull);
	}

	bindInicial() {
		return this.a;
	}

}
CampoConsultaCpf.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.IGUAL
);
