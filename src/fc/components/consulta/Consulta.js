import Ambiente from '../../config/Ambiente';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsultaBoolean from './CampoConsultaBoolean';
import CampoConsultaBotao from './CampoConsultaBotao';
import CampoConsultaCep from './CampoConsultaCep';
import CampoConsultaCpf from './CampoConsultaCpf';
import CampoConsultaDate from './CampoConsultaDate';
import CampoConsultaDecimal from './CampoConsultaDecimal';
import CampoConsultaEmail from './CampoConsultaEmail';
import CampoConsultaFk from './CampoConsultaFk';
import CampoConsultaInteger from './CampoConsultaInteger';
import CampoConsultaList from './CampoConsultaList';
import CampoConsultaLong from './CampoConsultaLong';
import CampoConsultaMoney from './CampoConsultaMoney';
import CampoConsultaSenha from './CampoConsultaSenha';
import CampoConsultaString from './CampoConsultaString';
import Console from '../../../app/misc/utils/Console';
import Null from '../../../commom/utils/object/Null';
import SeparacaoDeGrupo from './SeparacaoDeGrupo';
import Servico from '../../../projeto/Servico';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringPrimeiraMinuscula from '../../../commom/utils/string/StringPrimeiraMinuscula';
import VBoolean from '../../../app/campos/support/VBoolean';
import VInteger from '../../../app/campos/support/VInteger';
import VString from '../../../app/campos/support/VString';
import {message} from 'antd';

export default class Consulta extends VBoolean {

	campos = new ArrayLst();

	titleForm = new VString().setMaxLength(500);
	busca = new VString().setMaxLength(200).setLabel("busca");

	maisFiltros = false;

	pagina = new VInteger().setMaximo(9999).setMinimo(1).setLabel("Página");
	paginas = new VInteger().setMaximo(9999).setLabel("Páginas");
	registros = new VInteger().setMaximo(9999999).setLabel("Registros");

	init() {
		this.init2();
		this.nm = StringPrimeiraMinuscula.exec(this.nomeEntidade);
		this.campos.forEach(o => o.init());
	}

	refreshConsulta(resultadoConsulta) {

		if (!Null.is(resultadoConsulta.paginas)) {
			this.paginas.set(resultadoConsulta.paginas);
			this.registros.set(resultadoConsulta.registros);
			if (this.paginas.intValue() === 0) {
				Ambiente.showMessageInfo("A consulta não retornou nenhum resultado!");
				this.pagina.clear();
				this.afterRefreshConsulta();
				this.notifyObservers();
				return;
			}
		}

		this.pagina.set(resultadoConsulta.pagina);
		this.afterRefreshConsulta();
		setTimeout(() => {
			this.forceNotifyObservers();
			this.pagina.forceNotifyObservers();
			this.post("consulta-excel", this.getConsultaExcel());
		});

	}

	post(uriP, params) {
		return this.fcAxios.post(this.nomeEntidade + "/" + uriP, params, null);
	}

	afterRefreshConsulta() {}

	limparFiltros() {
		this.campos.forEach(o => o.clear());
	}

	changeVisibleCols() {
		if (this.campos.exists(o => o.visibleCol.isFalse())) {
			this.campos.forEach(o => o.visibleCol.set(true));
		} else {
			this.campos.forEach(o => o.visibleCol.set(false));
		}
	}

	changeMaisFiltros() {
		this.maisFiltros = !this.maisFiltros;
		this.notifyObservers();
	}

	isMaisFiltros() {
		return this.maisFiltros;
	}

	getBusca() {
		return this.busca;
	}

	consultar() {
		if (this.campos.exists(o => !o.isValid())) {
			Ambiente.showMessageError("Existem campos inválidos!");
			this.notifyObservers();
		} else {
			this.consultarImpl();
		}
	}

	add(campo) {
		this.campos.add(campo);
		return campo;
	}
	newString(nomeCampoP, nomeP, sizeP, notNull) {
		return this.add(new CampoConsultaString(nomeCampoP, nomeP, sizeP, notNull));
	}
	newEmail(nomeCampoP, nomeP, notNull) {
		return this.add(new CampoConsultaEmail(nomeCampoP, nomeP, notNull));
	}
	newSenha(nomeCampoP, nomeP, notNull) {
		return this.add(new CampoConsultaSenha(nomeCampoP, nomeP, notNull));
	}
	newCpf(nomeCampoP, nomeP, notNull) {
		return this.add(new CampoConsultaCpf(nomeCampoP, nomeP, notNull));
	}
	newCep(nomeCampoP, nomeP, notNull) {
		return this.add(new CampoConsultaCep(nomeCampoP, nomeP, notNull));
	}
	newList(nomeCampoP, nomeP, itensP, notNull) {
		return this.add(new CampoConsultaList(nomeCampoP, nomeP, itensP, notNull));
	}
	newSeparacaoDeGrupo(nomeCampoP, nomeP) {
		return this.add(new SeparacaoDeGrupo(nomeCampoP, nomeP));
	}
	newFk(nomeCampoP, nomeP, entidadeP, notNull) {
		return this.add(new CampoConsultaFk(nomeCampoP, nomeP, entidadeP, notNull, () => this.fcAxios));
	}
	newData(nomeCampoP, nomeP, notNull) {
		return this.add(new CampoConsultaDate(nomeCampoP, nomeP, notNull));
	}
	newInteger(nomeCampoP, nomeP, max, notNull) {
		return this.add(new CampoConsultaInteger(nomeCampoP, nomeP, max, notNull));
	}
	newBoolean(nomeCampoP, nomeP, notNull) {
		return this.add(new CampoConsultaBoolean(nomeCampoP, nomeP, notNull));
	}
	newDecimal(nomeCampoP, nomeP, inteiros, decimais, notNull) {
		return this.add(new CampoConsultaDecimal(nomeCampoP, nomeP, inteiros, decimais, notNull));
	}
	newNumeric(nomeCampoP, nomeP, inteiros, notNull) {
		return this.add(new CampoConsultaMoney(nomeCampoP, nomeP, inteiros, notNull));
	}
	newLong(nomeCampoP, nomeP, max, notNull) {
		return this.add(new CampoConsultaLong(nomeCampoP, nomeP, max, notNull));
	}
	newBotao(nomeCampoP, nomeP) {
		return this.add(new CampoConsultaBotao(nomeCampoP, nomeP));
	}

	toJsonImpl() {

		let x = this.campos.reduce((s, o) => {

			if (o.isTodos()) {
				return s;
			}

			if (o.semBinding()) {}

			let s2 = o.toString();
			if (!StringEmpty.is(s2)) {
				s += ", " + s2;
			}
			return s;

		}, "");

		let s = "{\"pagina\": " + this.pagina.get() + "";

		if (!StringEmpty.is(x)) {
			s += x;
		}

		s += "}";

		s = s.replace("undefined", "null");

		return s;
	}

	priorPage() {
		if (this.pagina.intValue() < 2) {
			return;
		}
		this.setPagina(this.pagina.get()-1);
	}

	setPagina(value) {
		this.pagina.disableObservers();
		this.pagina.set(value);
		this.pagina.enableObservers();
		this.consultarImpl();
	}

	nextPage() {
		if (this.pagina.intValue() >= this.paginas.get()) {
			return;
		}
		this.setPagina(this.pagina.get()+1);
	}

	lastPage() {
		if (this.pagina.intValue() !== this.paginas.intValue()) {
			this.pagina.set(this.paginas.intValue());
			this.consultarImpl();
		}
	}

	firstPage() {
		if (this.pagina.intValue() > 1) {
			this.pagina.set(1);
			this.consultarImpl();
		}
	}

	setGetCols(value) {
		this.getCols = value;
	}

	downloadExcel() {
		this.consultandoExcel.set(true);
		this.post("consulta-excel", this.getConsultaExcel())
			.then(res => {
				this.download(res);
				return res;
			})
			.catch(e => message.error("Erro ao tentar fazer download do arquivo: " + e.message))
		;

	}

	getConsultaExcel() {

		let obj = {};
		obj.groups = this.getCols.getGrupos().mapi((o,i) => {
			return {titulo: o.title, cols: o.cols};
		}).getArray();

		obj.cols = this.getCols.getList().mapi((o,i) => {
			return {titulo: o.title, campo: o.campo};
		}).getArray();

		obj.consulta = this;

		return obj;

	}

	download(res) {
		let o = res.body;
		if (StringCompare.eq(o.text, "2")) {
			this.consultandoExcel.set(false);
			let url = Servico.getImpl().getUriBase() + this.nm + "/download-excel/" + o.id;
			this.log(url);
			window.open(url);
		} else {
			setTimeout(() => {
				o.text = ""+Date.now();
				this.post("consulta-excel-status", o).then(resp => {
					this.download(resp);
					return resp;
				})
				.catch(e => Console.error("Erro ao tentar fazer download do arquivo", e.message));
			}, 500);
		}
	}

	outrosBotoes() {
		return Null.is(this.getOutrosBotoes) ? null : this.getOutrosBotoes();
	}

}
