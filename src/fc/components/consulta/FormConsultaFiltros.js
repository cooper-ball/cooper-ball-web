import React from 'react';
import CampoConsultaWebRenders from './CampoConsultaWebRenders';
import CommonStyles from '../../../app/misc/styles/CommonStyles';
import ConsultaStyles from './ConsultaStyles';
import Div from '../../../web/Div';
import FcBotao from '../FcBotao';
import InputBind from '../../../antd/InputBind';
import Obrig from '../../../commom/utils/object/Obrig';
import SeparacaoDeGrupo from './SeparacaoDeGrupo';
import StringLike from '../../../commom/utils/string/StringLike';
import Style from '../../../app/misc/utils/Style';
import SuperComponent from '../../../app/misc/components/SuperComponent';
import Tb from '../../../web/Tb';
import Td from '../../../web/Td';
import TextAlign from '../../../app/misc/consts/enums/TextAlign';
import Tr from '../../../web/Tr';
import VString from '../../../app/campos/support/VString';
import {SearchOutlined} from '@ant-design/icons';

export default class FormConsultaFiltros extends SuperComponent {

	filtro = new VString().setMaxLength(50).withPlaceHolder("Filtrar campos de pesquisa");

	limparFiltros() {
		this.props.consulta.limparFiltros();
		this.filtro.clear();
	}

	consultar() {
		this.props.consulta.consultar();
	}

	render0() {
		return (
			<Tb style={FormConsultaFiltros.TABLE_STYLE} className={"table-filtros"}>
				{this.garantirLarguraDasColunas()}
				{this.botoesSuperiores()}
				{this.acaoEmMassa()}
				{this.table()}
				{this.botoesInferiores()}
				{this.garantirLarguraDasColunas()}
			</Tb>
		);
	}

	garantirLarguraDasColunas() {
		return (
			<Tr>
				<Td style={ConsultaStyles.COL_A}/>
				<Td style={ConsultaStyles.COL_B}/>
				<Td style={ConsultaStyles.COL_C}/>
				<Td style={ConsultaStyles.COL_D}/>
				<Td style={ConsultaStyles.COL_E}/>
				<Td style={ConsultaStyles.COL_F}/>
				<Td style={ConsultaStyles.COL_G}/>
			</Tr>
		);
	}

	acaoEmMassa() {
		return (
			<Tr>
				<Td style={ConsultaStyles.COL_A}/>
				<Td style={ConsultaStyles.COL_B}/>
				<Td style={ConsultaStyles.COL_C}/>
				<Td style={ConsultaStyles.COL_D}/>
				<Td style={ConsultaStyles.COL_E}/>
				<Td style={ConsultaStyles.COL_F}/>
				<Td style={ConsultaStyles.COL_G}>
				{}</Td>
			</Tr>
		);
	}

	botoesSuperiores() {
		return (
			<Tr>
				<Td colSpan={2} style={ConsultaStyles.COL_AB}>
					{this.inputFiltro()}
				</Td>
				<Td colSpan={5} style={ConsultaStyles.COL_BCDEF}>
					<Div style={FormConsultaFiltros.STYLE_BUTTONS}>
						<FcBotao
							title={"Limpar Filtros"}
							acao={() => this.limparFiltros()}
							style={Style.create().marginRight(5)}
						/>
						{this.botaoConsultar()}
					</Div>
				</Td>
			</Tr>
		);
	}

	botoesInferiores() {
		return (
			<Tr>
				<Td colSpan={7}>
					<Div style={FormConsultaFiltros.STYLE_BUTTONS}>
						{this.botaoConsultar()}
					</Div>
				</Td>
			</Tr>
		);
	}

	botaoConsultar() {
		return (
			<FcBotao
				title={"Consultar (Ctrl + Enter)"}
				acao={() => this.consultar()}
			/>
		);
	}

	table() {

		let itens = this.props.consulta.campos.filter(o => (o instanceof SeparacaoDeGrupo) || StringLike.is(o.titulo, this.filtro.get()));

		if (itens.isEmpty()) {
			return null;
		} else {
			return this.map(itens, o => o.render0());
		}

	}

	inputFiltro() {
		return (
			<InputBind
				bind={this.filtro}
				after={<SearchOutlined/>}
			/>
		);
	}

	didMount() {
		this.observar(Obrig.check(this.props.consulta));
		this.observar(this.filtro);
	}

	getTempoAceitavelParaRenderizacao() {
		return 500;
	}
}
FormConsultaFiltros.STYLE_BUTTONS = Style.create().textAlign(TextAlign.right).marginTop(10).marginBottom(10);
FormConsultaFiltros.TABLE_STYLE = CommonStyles.W100P.copy().fontSize(CampoConsultaWebRenders.fontSize);

FormConsultaFiltros.defaultProps = SuperComponent.defaultProps;
