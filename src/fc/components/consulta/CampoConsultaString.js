import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VString from '../../../app/campos/support/VString';

export default class CampoConsultaString extends CampoConsulta {

	constructor(nomeCampoP, titulo, size, notNull) {
		super(nomeCampoP, titulo, CampoConsultaString.OPERADORES_POSSIVEIS, notNull);
		this.a = new VString().setMaxLength(size);
		this.setDefaultValue(ConsultaOperadorConstantes.COMECA_COM);
	}

	bindInicial() {
		return this.a;
	}

}
CampoConsultaString.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.COMECA_COM
	, ConsultaOperadorConstantes.CONTEM
	, ConsultaOperadorConstantes.TERMINA_COM
	, ConsultaOperadorConstantes.IGUAL
);
