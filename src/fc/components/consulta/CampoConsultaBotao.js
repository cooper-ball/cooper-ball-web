import CampoConsulta from './CampoConsulta';

export default class CampoConsultaBotao extends CampoConsulta {

	constructor(nomeCampoP, titulo) {
		super(nomeCampoP, titulo, null, false);
	}

	bindInicial() {
		return null;
	}

}
