import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VInteger from '../../../app/campos/support/VInteger';

export default class CampoConsultaInteger extends CampoConsulta {

	constructor(nomeCampoP, titulo, max, notNull) {
		super(nomeCampoP, titulo, CampoConsultaInteger.OPERADORES_POSSIVEIS, notNull);
		this.a = new VInteger().setMaximo(max);
		this.b = new VInteger().setMaximo(max);
	}

	bindInicial() {
		return this.a;
	}

	bindFinal() {
		return this.b;
	}

	valorInicialMaiorQueValorFinal() {
		return this.a.maiorQue(this.b);
	}

	render1() {
		return CampoConsultaInteger.renders.render0Integer(this);
	}

}
CampoConsultaInteger.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.IGUAL
	, ConsultaOperadorConstantes.MAIOR_OU_IGUAL
	, ConsultaOperadorConstantes.MENOR_OU_IGUAL
	, ConsultaOperadorConstantes.ENTRE
);
