import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import Null from '../../../commom/utils/object/Null';
import VArray from '../../../app/campos/support/VArray';

export default class CampoConsultaFk extends CampoConsulta {

	a = new VArray();

	chamada = 0;

	constructor(nomeCampoP, titulo, uri, notNull, getAxios) {
		super(nomeCampoP, titulo, CampoConsultaFk.OPERDORES_POSSIVEIS, notNull);
		this.getAxios = getAxios;
		this.uri = uri + "/consulta-select";
	}

	bindInicial() {
		return this.a;
	}

	input(o) {
		return CampoConsultaFk.renders.inputFk(this, this.a);
	}

	search(filtro) {
		this.a.setItens(CampoConsultaFk.ITENS_CLEAM);
		let cham = ++this.chamada;
		if (Null.is(filtro)) {
			return;
		}
		setTimeout(() => {
			if (IntegerCompare.eq(this.chamada, cham)) {
				let params = {};
				params.text = filtro;
				params.ignorar = this.a.get();
				this.getAxios().post(this.uri, params, res => {
					let resultadoConsulta = res.body;
					let array = resultadoConsulta.dados;
					this.a.setItens(new ArrayLst(array));
				});
			}
		}, 250);
	}

	toStringBind(bind) {
		return this.a.get().map(o => o.id).join(",");
	}

}
CampoConsultaFk.OPERDORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.EM
);
CampoConsultaFk.ITENS_CLEAM = new ArrayLst();
