import React from 'react';
import Afters from '../../../antd/form/Afters';
import BotaoAuditoria from '../../../fc/components/auditoria/BotaoAuditoria';
import BotaoSize from '../../../antd/BotaoSize';
import BotaoType from '../../../antd/BotaoType';
import Div from '../../../web/Div';
import FcBotao from '../../../fc/components/FcBotao';
import FormComponentErrorBinding from '../../../antd/form/FormComponentErrorBinding';
import FormEdit from '../../../fc/components/FormEdit';
import FormItemInput from '../../../antd/form/FormItemInput';
import GroupCard from '../../../fc/components/GroupCard';
import ModalAuditoria from '../../../fc/components/auditoria/ModalAuditoria';
import StringCompare from '../../../commom/utils/string/StringCompare';
import Tabela from '../../../fc/components/tabela/Tabela';
import Td from '../../../web/Td';
import TelefoneEdit from '../telefone/TelefoneEdit';
import {Col} from 'antd';
import {Row} from 'antd';
import {Tabs} from 'antd';
const TabPane = Tabs.TabPane;

export default class TorcedorEdit extends FormEdit {

	constructor(props) {
		super(props);
		this.init("Torcedor");
	}

	getTabs() {
		return (
			<Tabs
			tabPosition={"top"}
			activeKey={this.state.abaSelecionada}
			defaultActiveKey={"Geral"}
			onChange={s => this.setAbaSelecionada(s)}>
				<TabPane key={"Geral"} tab={"Geral"}>
					{this.abaGeral()}
				</TabPane>
			</Tabs>
		);
	}

	abaGeral() {
		if (!StringCompare.eq(this.state.abaSelecionada, "Geral")) {
			return null;
		}
		return (
			<Row gutter={24}>
				{this.grupo_geral_geral()}
				{this.grupo_geral_enderecoResidencial()}
				{this.grupo_geral_telefones()}
			</Row>
		);
	}

	inputCpf() {
		return <FormItemInput bind={this.campos.cpf} lg={6}/>;
	}

	inputNome() {
		return <FormItemInput bind={this.campos.nome} lg={12}/>;
	}

	inputEmail() {
		return <FormItemInput after={Afters.getEmail()} bind={this.campos.email} lg={6}/>;
	}

	grupo_geral_geral() {
		if (!this.campos.cpf.isVisible() && !this.campos.nome.isVisible() && !this.campos.email.isVisible()) {
			return null;
		}
		return (
			<Col lg={24} md={24} sm={24}>
				<GroupCard title={"Geral"}>
					<Row gutter={24}>
						{this.inputCpf()}
						{this.inputNome()}
						{this.inputEmail()}
					</Row>
				</GroupCard>
			</Col>
		);
	}

	inputComplemento() {
		return <FormItemInput bind={this.campos.complemento} lg={12}/>;
	}

	inputNumero() {
		return <FormItemInput bind={this.campos.numero} lg={12}/>;
	}

	grupo_geral_enderecoResidencial() {
		if (!this.campos.cep.isVisible() && !this.campos.complemento.isVisible() && !this.campos.numero.isVisible()) {
			return null;
		}
		return (
			<Col lg={24} md={24} sm={24}>
				<GroupCard title={"Endereço Residencial"}>
					<Row gutter={24}>
						<FormItemInput bind={this.campos.cep} lg={4}/>
						<FormItemInput bind={this.campos.cep.bairroLocalidadeUf} lg={12}/>
						<FormItemInput bind={this.campos.cep.logradouro} lg={8}/>
					</Row>
					<Row gutter={24}>
						{this.inputComplemento()}
						{this.inputNumero()}
					</Row>
				</GroupCard>
			</Col>
		);
	}

	botaoNovoTelefones() {
		if (this.permissoes.podeIncluirTelefones()) {
			return <FcBotao title={"+ Novo"} type={BotaoType.normal} size={BotaoSize.small} acao={() => this.campos.telefonesNovo()}/>;
		} else {
			return null;
		}
	}

	onDeleteTelefones() {
		if (this.permissoes.podeExcluirTelefones()) {
			return o => {
				o.excluido = true;
				this.campos.telefones.forceNotifyObservers();
			};
		} else {
			return null;
		}
	}

	grupo_geral_telefones() {
		if (!this.campos.telefones.isVisible()) {
			return null;
		}
		return (
			<Col lg={24} md={24} sm={24}>
				<GroupCard title={"Telefones"} extra={this.botaoNovoTelefones()}>
					<Row gutter={24}>
						<Div style={this.newStyle().paddingLeftRight(15).w100()}>
							<Tabela
							bind={this.campos.telefones}
							onClick={o => this.campos.telefonesEdit(o)}
							onDelete={this.onDeleteTelefones()}
							colunas={this.torcedorTelefonesCols.getList()}
							colunasGrupo={this.torcedorTelefonesCols.getGrupos()}
							/>
							<FormComponentErrorBinding bind={this.campos.telefones}/>
						</Div>
					</Row>
				</GroupCard>
			</Col>
		);
	}

	getTitleImpl() {
		return "Torcedor";
	}

	getModal() {
		if (this.campos.auditoria.modalAuditoria.isTrue()) {
			return <ModalAuditoria auditoria={this.campos.auditoria}/>;
		}
		if (this.campos.telefones.isTrue()) {
			return (
				<TelefoneEdit
				vinculo={this.campos.telefones}
				somenteUpdate={false}
				isModal={true}
				/>
			);
		}
		throw new Error("???");
	}

	getCampos() {
		return this.campos;
	}

	getPermissoes() {
		return this.permissoes;
	}

	botoesSuperiores() {
		return (
			<Td style={FormEdit.TD_BUTTONS_RIGHT}>
				<BotaoAuditoria auditoria={this.campos.auditoria}/>
			</Td>
		);
	}

	didMount2() {
		super.didMount2();
		this.observar(this.campos.auditoria.modalAuditoria);
	}

	esteFormEstahExibindoAlgumModal() {
		if (this.campos.auditoria.modalAuditoria.isTrue()) {
			return true;
		}
		return super.esteFormEstahExibindoAlgumModal();
	}
	setWidthForm = o => this.setState({widthForm:o});
	setAbaSelecionada = o => this.setState({abaSelecionada:o});
}

TorcedorEdit.defaultProps = FormEdit.defaultProps;
