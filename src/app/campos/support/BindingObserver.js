import ClassSimpleName from '../../../commom/utils/classe/ClassSimpleName';
import Console from '../../misc/utils/Console';
import Context from '../../misc/components/Context';
import Equals from '../../../commom/utils/object/Equals';
import Null from '../../../commom/utils/object/Null';
import ObjJs from '../../../commom/utils/object/ObjJs';
import StartPrototypes from '../../misc/utils/StartPrototypes';
import Uncycle from '../../misc/components/Uncycle';

export default class BindingObserver extends ObjJs {

	static idComponentCount = 0;
	idComponent = BindingObserver.idComponentCount++;

	constructor() {
		super();
		StartPrototypes.exec();
		this.thisClassName = ClassSimpleName.exec(this);
		Context.injetar(this);
	}

	getIdComponent() {
		return this.idComponent;
	}
	notify0(o) {}

	focus() {
		Uncycle.setSelectedCombo(null);
		if (!Null.is(this.focusController)) {
			let focused = this.focusController.get();
			if (Equals.is(focused, this)) {
				return;
			}
			this.focusController.set(this);
			if (!Null.is(this.onFocus)) {
				this.onFocus();
			}
			if (!Null.is(focused) && !Null.is(focused.onBlur)) {
				focused.onBlur();
			}
		}
	}

	notifyObservers() {}

	log(o) {
		Console.log(this.thisClassName, o);
	}

	logRrror(o) {
		Console.error(this.thisClassName, o);
	}

	toString() {
		return ClassSimpleName.exec(this)+"-"+this.idComponent;
	}
}
