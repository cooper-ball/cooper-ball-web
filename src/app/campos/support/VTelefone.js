import UTelefone from '../../misc/utils/UTelefone';
import VString from './VString';

export default class VTelefone extends VString {

	casas = 9;

	afterConstruct() {
		this.setMaxLength(20);
	}

	setCasas(value) {
		this.casas = value;
		this.setMaxLength(UTelefone.formatParcial("61992559810992559810", value).length);
		this.set(this.get());
	}

	getInvalidMessagePrivate() {
		if (UTelefone.isValid(this.get(), this.casas)) {
			return null;
		} else {
			return "Telefone inválido!";
		}
	}

	formatParcial(s) {
		return UTelefone.formatParcial(s, this.casas);
	}

	isNumeric() {
		return true;
	}

}
