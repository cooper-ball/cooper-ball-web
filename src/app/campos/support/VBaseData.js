import BaseData from '../../../commom/utils/date/BaseData';
import Binding from './Binding';

export default class VBaseData extends Binding {

	setNow() {
		this.set(BaseData.now());
	}

}
