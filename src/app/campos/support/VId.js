import Null from '../../../commom/utils/object/Null';
import VInteger from './VInteger';

export default class VId extends VInteger {

	afterConstruct() {
		this.setLabel("id");
		this.setMaximo(999999999);
	}

	beforeSet(x) {
		if (!this.isEmpty() && !Null.is(x) && !this.eq(x)) {
			throw new Error("Mudança de Id: de " + this.get() + " para " + x);
		}
		return super.beforeSet(x);
	}

}
