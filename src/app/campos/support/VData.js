import BaseData from '../../../commom/utils/date/BaseData';
import moment from 'moment';
import Null from '../../../commom/utils/object/Null';
import StringAfterFirst from '../../../commom/utils/string/StringAfterFirst';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringParse from '../../../commom/utils/string/StringParse';
import UData from '../../misc/utils/UData';
import VString from './VString';

export default class VData extends VString {

	modalVisible = false;

	afterConstruct() {
		this.setMaxLength(10);
	}

	getInvalidMessagePrivate() {
		let s = this.get();
		if (UData.isValida(s)) {
			return this.getInvalidMessagePrivate2();
		} else if (s.length < 10) {
			return "Incompleta (deve conter 10 carateres)";
		} else {
			return "Inválida!";
		}
	}

	getInvalidMessagePrivate2() {
		return null;
	}

	formatParcial(s) {
		return UData.formatParcial(s);
	}

	toDate() {
		if (!this.isValid()) {
			return null;
		}
		let s = this.get();
		let dia = parseInt(s.substring(0, 2));
		s = StringAfterFirst.get(s, "/");
		let mes = parseInt(s.substring(0, 2))-1;
		s = StringAfterFirst.get(s, "/");
		let ano = parseInt(s);
		let date = new Date(ano, mes, dia, 0, 0, 0, 0);
		return date;
	}

	toBaseData() {
		if (this.isValid()) {
			return BaseData.unformat("[dd]/[mm]/[yyyy]", this.get());
		} else {
			return null;
		}
	}

	getToServiceImpl() {
		if (this.isValid()) {
			return this.toBaseData().format("[yyyy]/[mm]/[dd]");
		} else {
			return null;
		}
	}

	isNumeric() {
		return true;
	}

	setDate(value) {
		if (Null.is(value)) {
			this.clear();
		} else if (StringCompare.eq(StringParse.get(value), "null")) {
			this.clear();
		} else if (value instanceof Date) {
			this.set2(UData.toData(value));
		} else {
			this.clear();
		}
	}

	setMoment(value) {
		if (Null.is(value)) {
			this.clear();
		} else {
			this.set(value.format("DDMMYYYY"));
		}
	}

	getMoment() {
		if (this.isValid() && !this.isEmpty()) {
			return moment(this.get(), "DD/MM/YYYY");
		} else {
			return null;
		}
	}

	menorQue(b) {

		if (this.isEmpty()) {
			return !b.isEmpty();
		} else if (b.isEmpty()) {
			return false;
		} else if (this.eq(b.get())) {
			return false;
		} else if (!this.isValid() || !b.isValid()) {
			return false;
		}

		return this.toDate().getTime() < b.toDate().getTime();

	}

	maiorQue(b) {

		if (this.isEmpty()) {
			return !b.isEmpty();
		} else if (b.isEmpty()) {
			return false;
		} else if (this.eq(b.get())) {
			return false;
		} else if (!this.isValid() || !b.isValid()) {
			return false;
		}

		return this.toDate().getTime() > b.toDate().getTime();

	}

	eq2(o) {
		if (Null.is(o)) {
			return this.isEmpty();
		} else {
			return o.eq(this.toBaseData());
		}
	}

	set2(baseData) {
		return this.set(baseData.format("[dd]/[mm]/[yyyy]"));
	}

	setBaseData(o) {
		if (Null.is(o)) {
			return this.clear();
		} else {
			return this.set(o.format("[dd]/[mm]/[yyyy]"));
		}
	}

	setHoje() {
		return this.set2(UData.hoje());
	}

	setOntem() {
		let o = BaseData.hoje();
		o.removeDia();
		return this.setBaseData(o);
	}

	setModalVisible(value) {
		if (value !== this.modalVisible) {
			this.modalVisible = value;
			this.forceNotifyObservers();
		}
	}

	isModalVisible() {
		return this.modalVisible;
	}

}
