import ArrayLst from '../../../commom/utils/array/ArrayLst';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import VList from './VList';

export default class VFk extends VList {

	chamandoServico = false;
	chamada = 0;

	constructor(labelP, service) {
		super();
		this.service = service;
		this.label = labelP;
	}

	notify(o) {
		super.notify(o);
		if (this.isDisabled()) {
			return;
		}
		this.chamandoServico = true;
		let cham = ++this.chamada;
		setTimeout(() => {
			if (IntegerCompare.eq(this.chamada, cham)) {
				this.service.exec(this.input.get(), res => {
					let array = res.body;
					let list = new ArrayLst(array);
					this.chamandoServico = false;
					this.setItens(list);
				});
			}
		}, 250);
	}

	getSugestoes() {
		if (this.chamandoServico) {
			return new ArrayLst();
		} else {
			return this.getItens().map(o => {
				return {value: o};
			});
		}
	}

}
