import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import IntegerFormat from '../../../commom/utils/integer/IntegerFormat';
import IntegerIs from '../../../commom/utils/integer/IntegerIs';
import Null from '../../../commom/utils/object/Null';
import SeparaMilhares from '../../../commom/utils/comum/SeparaMilhares';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringSplit from '../../../commom/utils/string/StringSplit';
import UCommons from './UCommons';

export default class UInteger {

	static isPositivo(o) {
		if (Null.is(o)) {
			return false;
		} else {
			return o > 0;
		}
	}

	static isNegativo(o) {
		if (Null.is(o)) {
			return false;
		} else {
			return o < 0;
		}
	}

	static isEmptyOrZero(o) {
		return Null.is(o) || UCommons.equals(o, 0);
	}

	static between(value, min, max) {
		return value >= min && value <= max;
	}
	static compareToInt(a, b) {
		return UInteger.compare(UInteger.toInt(a), UInteger.toInt(b));
	}
	static compareCast(a, b) {
		return UInteger.compare(UCommons.cast(a), UCommons.cast(b));
	}
	static compareCastReverse(a, b) {
		return UInteger.compare(UCommons.cast(a), UCommons.cast(b)) * -1;
	}
	static compare(a, b) {
		if (UCommons.equals(a, b)) {
			return 0;
		} else if (Null.is(a)) {
			return -1;
		} else if (Null.is(b)) {
			return 1;
		} else if (a < b) {
			return -1;
		} else if (b < a) {
			return 1;
		} else {
			return 0;
		}
	}

	static format00(value, casas) {
		return IntegerFormat.zerosEsquerda(value, casas);
	}

	static equals(a, b) {
		return IntegerCompare.eq(a, b);
	}

	static main(args) {
		console.log(UInteger.separarMilhares("1234567"));
	}

	static separarMilhares(s) {
		s = StringExtraiNumeros.exec(s);
		if (StringEmpty.is(s)) {
			return "";
		}
		let split = StringSplit.exec(s, "");
		s = "";
		while (!split.isEmpty()) {
			s = split.pop() + s;
			if (!split.isEmpty()) {
				s = split.pop() + s;
				if (!split.isEmpty()) {
					s = split.pop() + s;
					if (!split.isEmpty()) {
						s = "." + s;
					}
				}
			}
		}
		return s;
	}
	static format(value) {
		if (UInteger.isEmptyOrZero(value)) {
			return "";
		} else {
			return SeparaMilhares.exec(""+value);
		}
	}

	static toIntDef(o, def) {
		if (IntegerIs.is(o)) {
			return UInteger.toInt(o);
		}
		try {
			o = parseInt(o);
			if (IntegerIs.is(o)) {
				return UInteger.toInt(o);
			} else {
				return def;
			}
		} catch (e) {
			return def;
		}

	}

	static toInt(o) {
		if (Null.is(o)) {
			return null;
		} else {
			return parseInt(o);
		}
	}

	static isInt(o) {
		return IntegerIs.is(o);
	}
}
