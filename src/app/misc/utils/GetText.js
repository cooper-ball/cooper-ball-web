import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringParse from '../../../commom/utils/string/StringParse';
import StringPrimeiraMaiuscula from '../../../commom/utils/string/StringPrimeiraMaiuscula';
import UBoolean from './UBoolean';
import UCommons from './UCommons';

export default class GetText {

	static getAttr(o, attr) {

		let pm = StringPrimeiraMaiuscula.exec(attr);

		let getText = o["get" + pm];
		if (!Null.is(getText)) {
			let x = o;
			x.getText = getText;
			return x.getText();
		}

		let text = o[attr];

		if (UCommons.neq(text, undefined)) {
			return text;
		}

		return null;

	}

	static get(o) {

		if (Null.is(o)) {
			return null;
		}
		if (StringCompare.eq(typeof(o), "string")) {
			return o;
		}
		if (StringCompare.eq(typeof(o), "boolean")) {
			return UBoolean.isTrue(o) ? "Sim" : "Não";
		}

		if (StringCompare.eq(typeof(o), "number")) {
			return StringParse.get(o);
		}

		let s = GetText.getAttr(o, "text");
		if (!StringEmpty.is(s)) return s;

		s = GetText.getAttr(o, "nome");
		if (!StringEmpty.is(s)) return s;

		s = GetText.getAttr(o, "descricao");
		if (!StringEmpty.is(s)) return s;

		s = GetText.getAttr(o, "codigo");
		if (!StringEmpty.is(s)) return s;

		s = GetText.getAttr(o, "numero");
		if (!StringEmpty.is(s)) return s;

		return null;

	}
}
