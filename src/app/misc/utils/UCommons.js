import ClassSimpleName from '../../../commom/utils/classe/ClassSimpleName';
import Equals from '../../../commom/utils/object/Equals';
import IdText from '../../../commom/utils/object/IdText';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringParse from '../../../commom/utils/string/StringParse';

export default class UCommons {

	static notEmptyId(o) {
		return !UCommons.isEmptyId(o);
	}
	static isEmptyId(o) {

		if (Null.is(o)) {
			return true;
		}

		if (UCommons.eq(o, 0)) {
			return true;
		}

		if (o instanceof IdText) {
			let x = o;
			return UCommons.eq(x.id, 0);
		}

		return false;

	}

	static notEmpty(o) {
		return !Null.is(o);
	}

	static isEmpty(o) {
		return Null.is(o);
	}
	static notEquals(a, b) {
		return !UCommons.equals(a, b);
	}

	static inEquals = false;

	static equals(a, b) {

		if (UCommons.inEquals) {
			return false;
		}

		UCommons.inEquals = true;

		try {

			if (UCommons.eq(a,b)) {
				return true;
			}

			if (Null.is(a)) {
				return Null.is(b);
			} else if (Null.is(b)) {
				return false;
			}

			if (a instanceof IdText) {
				let x = a;
				a = x.id;
			}

			if (b instanceof IdText) {
				let x = b;
				b = x.id;
			}
			if (UCommons.eq(a,b)) {
				return true;
			}
			if (ClassSimpleName.exec(a) !== ClassSimpleName.exec(b) ) {
				return false;
			}

			let sa = StringParse.get(a);
			let sb = StringParse.get(b);

			return UCommons.eq(sa,sb);

		} finally {
			UCommons.inEquals = false;
		}

	}
	static eqeqeq(a, b) {
		if (Null.is(a)) return Null.is(b);
		return a === b;
	}
	static neq(a, b) {
		return !Equals.is(a, b);
	}
	static eq(a, b) {
		return Equals.is(a, b) && StringCompare.eq(typeof(a), typeof(b));
	}

	static coalesce(a, b) {
		return Null.is(a) ? b : a;
	}

	static cast(o) {
		return o;
	}

	static isBoolean(o) {
		if (Null.is(o)) {
			return false;
		} else {
			return StringCompare.eq(typeof(o), "boolean");
		}
	}

	static isLink(s) {
		return s.startsWith("www.") || s.startsWith("http") || s.endsWith(".com.br");
	}

	static simpleCompare(a, b) {
		if (UCommons.equals(a, b)) {
			return 0;
		} else if (Null.is(a)) {
			return -1;
		} else if (Null.is(b)) {
			return 1;
		} else {
			return null;
		}
	}

	static instanceOf(o, classe) {
		let className = ClassSimpleName.exec(o);
		return UCommons.equals(className, classe.name);
	}

}
