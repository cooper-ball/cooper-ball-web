import React from 'react';
import ArrayLst from '../commom/utils/array/ArrayLst';
import Color from '../app/misc/consts/enums/Color';
import Null from '../commom/utils/object/Null';
import StringCompare from '../commom/utils/string/StringCompare';
import StringParse from '../commom/utils/string/StringParse';
import Style from '../app/misc/utils/Style';
import SuperComponent from '../app/misc/components/SuperComponent';
import UInteger from '../app/misc/utils/UInteger';
import {Select} from 'antd';
import {Tag} from 'antd';
const SelectOption = Select.Option;

export default class SelectMultipleBind extends SuperComponent {

	static STYLE_DEFAULT = Style.create();

	render0() {

		let value = this.props.bind.get().map(o => o.text);
		let options = this.props.bind.getItensNaoSelecionados().map(o => o.text);

		return (
			<Select
				mode={"multiple"}
				placeholder={this.props.placeholder}
				value={value.getArray()}
				onChange={values => this.setValues(new ArrayLst(values))}
				tagRender={o => this.renderTag(o)}
				style={SelectMultipleBind.STYLE_DEFAULT.join(this.props.style).get()}
				onSearch={this.props.onSearch}>
				{this.map(options, o => <SelectOption key={StringParse.get(o)} value={o}>{o}</SelectOption>)}
			</Select>
		);
	}

	setValues(values) {
		let array = values.map(o => this.getIdText(o));
		this.setValuesArray(array);
	}
	setValuesArray(array) {
		array.sort((a,b) => UInteger.compare(a.id, b.id));
		this.props.bind.set(array);
	}

	getIdText(s) {
		let o = this.props.bind.getItens().unique(x => StringCompare.eq(x.text, s));
		if (!Null.is(o)) {
			return o;
		} else {
			return this.props.bind.get().uniqueObrig(x => StringCompare.eq(x.text, s));
		}
	}

	renderTag(o) {
		if (Null.is(this.props.tagRender)) {
			return (
				<Tag color={Color.blue} closable={true} onClose={() => {
					let array = this.props.bind.get().copy();
					array.removeObject(this.getIdText(o.value));
					this.setValuesArray(array);
				}}>{o.value}</Tag>
			);
		} else {
			return this.props.tagRender(this.getIdText(o.value));
		}
	}

	didMount() {
		this.observar(this.props.bind);
	}
}

SelectMultipleBind.defaultProps = SuperComponent.defaultProps;
