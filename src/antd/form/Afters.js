import React from 'react';
import CommonStyles from '../../app/misc/styles/CommonStyles';
import Null from '../../commom/utils/object/Null';
import Span from '../../web/Span';
import {message} from 'antd';

export default class Afters {

	static email;
	static alfa;
	static numeric;

	static getAlfa() {
		if (Null.is(Afters.alfa)) {
			Afters.alfa = Afters.get("Significa que o campo aceita letras e numeros", "A");
		}
		return Afters.alfa;
	}

	static getNumeric() {
		if (Null.is(Afters.numeric)) {
			Afters.numeric = Afters.get("Significa que o campo aceita somente numeros", "#");
		}
		return Afters.numeric;
	}

	static getEmail() {
		if (Null.is(Afters.email)) {
			Afters.email = Afters.get("Campo do tipo e-mail", "@");
		}
		return Afters.email;
	}

	static get(a, b) {
		return <Span onPress={() => message.info(a)} style={Afters.STYLE_ICON} text={b}/>;
	}

}
Afters.STYLE_ICON = CommonStyles.POINTER;
