import React from 'react';
import Color from '../../app/misc/consts/enums/Color';
import Div from '../../web/Div';
import Label from '../../web/Label';
import StringEmpty from '../../commom/utils/string/StringEmpty';
import Style from '../../app/misc/utils/Style';
import SuperComponent from '../../app/misc/components/SuperComponent';

export default class FormComponentError extends SuperComponent {

	render0() {

		return (
			<Div className={"ant-form-explain"}>
				{!StringEmpty.is(this.props.message) && <Label style={FormComponentError.LABEL_ERROR} text={this.props.message}/>}
			</Div>
		);

	}
}
FormComponentError.LABEL_ERROR = Style.create().color(Color.red).fontSize(10);

FormComponentError.defaultProps = SuperComponent.defaultProps;
