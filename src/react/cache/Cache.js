import ArrayLst from '../../commom/utils/array/ArrayLst';
import CacheItem from './CacheItem';
import IntegerParse from '../../commom/utils/integer/IntegerParse';
import Null from '../../commom/utils/object/Null';
import StringCompare from '../../commom/utils/string/StringCompare';

export default class Cache {

	static list = new ArrayLst();

	static find(uri, params) {
		return Cache.list.unique(i => StringCompare.eq(i.uri, uri) && StringCompare.eq(i.params, params));
	}

	static clearUri(uri) {
		Cache.list.removeIf(o => StringCompare.eq(o.uri, uri));
	}

	static get(uri, params, segundosExpiracao) {

		let o = Cache.find(uri, params);

		if (Null.is(o)) {
			return null;
		}

		let tempoPassado = IntegerParse.toInt((Date.now() - o.time) / 1000);

		if (tempoPassado > segundosExpiracao) {
			return null;
		} else {
			return o.response;
		}

	}

	static set(uri, params, value) {
		let o = Cache.find(uri, params);
		if (Null.is(o)) {
			o = new CacheItem();
			o.uri = uri;
			o.params = params;
			Cache.list.add(o);
		}
		o.response = value;
		o.time = Date.now();
	}

}
