import AmbienteConfigWeb from '../../fc/config/AmbienteConfigWeb';
import App from '../../App';
import ArrayLst from '../../commom/utils/array/ArrayLst';
import Authorization from '../../fc/state/login/Authorization';
import BotaoLogout from '../../fc/state/login/BotaoLogout';
import Cache from '../../fc/services/Cache';
import CacheClear from '../../fc/services/CacheClear';
import Calendario from '../../fc/components/Calendario';
import Configure from './Configure';
import ConsultandoExcel from '../../fc/components/consulta/excel/ConsultandoExcel';
import Context from '../../app/misc/components/Context';
import ControleSessao from '../../fc/state/login/ControleSessao';
import EfetuarLogin from '../../fc/state/login/EfetuarLogin';
import EfetuarLogout from '../../fc/state/login/EfetuarLogout';
import FcAxios from '../../fc/services/FcAxios';
import FcBotao from '../../fc/components/FcBotao';
import FormComponentErrorBinding from '../../antd/form/FormComponentErrorBinding';
import FormConsultaFiltros from '../../fc/components/consulta/FormConsultaFiltros';
import FormEditButtons from '../../fc/components/FormEditButtons';
import InputBind from '../../antd/InputBind';
import Loadings from '../../fc/loading/Loadings';
import LoadingView from '../../fc/loading/LoadingView';
import LocalStorage from '../../web/misc/LocalStorage';
import Logado from '../../fc/state/login/Logado';
import LoginView from '../../fc/state/login/LoginView';
import Mensagens from '../../fc/messages/Mensagens';
import MessageView from '../../fc/messages/MessageView';
import ModalCamposAlterados from '../../fc/components/campoAlterado/ModalCamposAlterados';
import OnStart from './OnStart';
import Paginacao from '../../fc/components/consulta/Paginacao';
import PermissoesDefault from '../../fc/components/PermissoesDefault';
import PermissoesMenu from '../../app/auto/outros/PermissoesMenu';
import PopoverDate from '../../fc/components/PopoverDate';
import PopoverErros from '../../fc/components/PopoverErros';
import SearchService from '../../fc/components/SearchService';
import SelectBind from '../../antd/SelectBind';
import SelectBindSimple from '../../antd/SelectBindSimple';
import SelectMultipleBind from '../../antd/SelectMultipleBind';
import Senha from '../../fc/state/login/Senha';
import SessionStorage from '../../web/misc/SessionStorage';
import Storage from '../../fc/state/storage/Storage';
import Tabela from '../../fc/components/tabela/Tabela';
import TelefoneCampos from '../../app/cruds/telefone/TelefoneCampos';
import TelefoneCols from '../../app/cruds/telefone/TelefoneCols';
import TelefoneConsulta from '../../app/cruds/telefone/TelefoneConsulta';
import TelefoneEdit from '../../app/cruds/telefone/TelefoneEdit';
import TelefoneFormConsulta from '../../app/cruds/telefone/TelefoneFormConsulta';
import TelefonePermissoes from '../../app/cruds/telefone/TelefonePermissoes';
import TelefoneTipoCampos from '../../app/cruds/telefoneTipo/TelefoneTipoCampos';
import TelefoneTipoCols from '../../app/cruds/telefoneTipo/TelefoneTipoCols';
import TelefoneTipoConsulta from '../../app/cruds/telefoneTipo/TelefoneTipoConsulta';
import TelefoneTipoEdit from '../../app/cruds/telefoneTipo/TelefoneTipoEdit';
import TelefoneTipoUtils from '../../app/cruds/telefoneTipo/TelefoneTipoUtils';
import TelefoneUtils from '../../app/cruds/telefone/TelefoneUtils';
import Tema from '../../fc/tema/Tema';
import TorcedorCampos from '../../app/cruds/torcedor/TorcedorCampos';
import TorcedorCols from '../../app/cruds/torcedor/TorcedorCols';
import TorcedorConsulta from '../../app/cruds/torcedor/TorcedorConsulta';
import TorcedorEdit from '../../app/cruds/torcedor/TorcedorEdit';
import TorcedorFormConsulta from '../../app/cruds/torcedor/TorcedorFormConsulta';
import TorcedorPermissoes from '../../app/cruds/torcedor/TorcedorPermissoes';
import TorcedorTelefonesCols from '../../app/cruds/torcedor/TorcedorTelefonesCols';
import TorcedorTelefoneValidations from '../../app/cruds/torcedor/TorcedorTelefoneValidations';
import TorcedorUtils from '../../app/cruds/torcedor/TorcedorUtils';
import Usuario from '../../fc/state/login/Usuario';
import VariaveisDeAmbiente from '../../fc/state/system/VariaveisDeAmbiente';
import VCamposAlterados from '../../fc/outros/VCamposAlterados';

export default class ReactContext {

	static ID_COUNT = 0;
	static criado = false;
	id = ReactContext.ID_COUNT++;
	inicializado = false;
	injetarFila = new ArrayLst();

	constructor() {
		if (ReactContext.criado) {
			throw new Error("ja criado");
		} else {
			ReactContext.criado = true;
		}
	}

	init() {
		this.localStorage = new LocalStorage();
		this.variaveisDeAmbiente = new VariaveisDeAmbiente();
		this.cacheClear = new CacheClear();
		this.tema = new Tema();
		this.consultandoExcel = new ConsultandoExcel();
		this.telefoneUtils = new TelefoneUtils();
		this.telefonePermissoes = new TelefonePermissoes();
		this.torcedorPermissoes = new TorcedorPermissoes();
		this.loadings = new Loadings();
		this.mensagens = new Mensagens();
		this.telefoneTipoUtils = new TelefoneTipoUtils();
		this.permissoesDefault = new PermissoesDefault();
		this.sessionStorage = new SessionStorage();
		this.storage = new Storage();
		this.torcedorUtils = new TorcedorUtils();
		this.ambienteConfigWeb = new AmbienteConfigWeb();
		this.authorization = new Authorization();
		this.senha = new Senha();
		this.usuario = new Usuario();
		this.logado = new Logado();
		this.cache = new Cache();
		this.fcAxios = new FcAxios();
		this.telefoneConsulta = new TelefoneConsulta();
		this.torcedorConsulta = new TorcedorConsulta();
		this.vCamposAlterados = new VCamposAlterados();
		this.efetuarLogout = new EfetuarLogout();
		this.efetuarLogin = new EfetuarLogin();
		this.permissoesMenu = new PermissoesMenu();
		this.telefoneTipoConsulta = new TelefoneTipoConsulta();
		this.telefoneCampos = new TelefoneCampos();
		this.torcedorCols = new TorcedorCols();
		this.telefoneCols = new TelefoneCols();
		this.telefoneTipoCampos = new TelefoneTipoCampos();
		this.telefoneTipoCols = new TelefoneTipoCols();
		this.controleSessao = new ControleSessao();
		this.torcedorCampos = new TorcedorCampos();
		this.torcedorTelefonesCols = new TorcedorTelefonesCols();
		this.torcedorTelefoneValidations = new TorcedorTelefoneValidations();
		this.storage.storage = this.localStorage;
		this.torcedorUtils.telefoneUtils = this.telefoneUtils;
		this.ambienteConfigWeb.lStorage = this.localStorage;
		this.ambienteConfigWeb.sStorage = this.sessionStorage;
		this.authorization.storage = this.storage;
		this.senha.storage = this.storage;
		this.usuario.storage = this.storage;
		this.logado.storage = this.storage;
		this.logado.authorization = this.authorization;
		this.cache.logado = this.logado;
		this.cache.cacheClear = this.cacheClear;
		this.fcAxios.cache = this.cache;
		this.fcAxios.logado = this.logado;
		this.fcAxios.authorization = this.authorization;
		this.fcAxios.variaveisDeAmbiente = this.variaveisDeAmbiente;
		this.telefoneConsulta.consultandoExcel = this.consultandoExcel;
		this.telefoneConsulta.fcAxios = this.fcAxios;
		this.telefoneConsulta.telefoneUtils = this.telefoneUtils;
		this.torcedorConsulta.consultandoExcel = this.consultandoExcel;
		this.torcedorConsulta.fcAxios = this.fcAxios;
		this.torcedorConsulta.torcedorUtils = this.torcedorUtils;
		this.vCamposAlterados.fcAxios = this.fcAxios;
		this.efetuarLogout.authorization = this.authorization;
		this.efetuarLogout.axios = this.fcAxios;
		this.efetuarLogin.restApi = this.fcAxios;
		this.efetuarLogin.usuario = this.usuario;
		this.efetuarLogin.senha = this.senha;
		this.efetuarLogin.loadings = this.loadings;
		this.efetuarLogin.mensagens = this.mensagens;
		this.efetuarLogin.authorization = this.authorization;
		this.efetuarLogin.axios = this.fcAxios;
		this.permissoesMenu.fcAxios = this.fcAxios;
		this.telefoneTipoConsulta.consultandoExcel = this.consultandoExcel;
		this.telefoneTipoConsulta.fcAxios = this.fcAxios;
		this.telefoneTipoConsulta.telefoneTipoUtils = this.telefoneTipoUtils;
		this.telefoneCampos.fcAxios = this.fcAxios;
		this.telefoneCampos.telefoneUtils = this.telefoneUtils;
		this.telefoneCampos.telefoneConsulta = this.telefoneConsulta;
		this.torcedorCols.torcedorConsulta = this.torcedorConsulta;
		this.telefoneCols.telefoneConsulta = this.telefoneConsulta;
		this.telefoneTipoCampos.fcAxios = this.fcAxios;
		this.telefoneTipoCampos.telefoneTipoUtils = this.telefoneTipoUtils;
		this.telefoneTipoCampos.telefoneTipoConsulta = this.telefoneTipoConsulta;
		this.telefoneTipoCols.telefoneTipoConsulta = this.telefoneTipoConsulta;
		this.controleSessao.logado = this.logado;
		this.controleSessao.storage = this.storage;
		this.controleSessao.efetuarLogout = this.efetuarLogout;
		this.torcedorCampos.fcAxios = this.fcAxios;
		this.torcedorCampos.telefoneUtils = this.telefoneUtils;
		this.torcedorCampos.torcedorUtils = this.torcedorUtils;
		this.torcedorCampos.torcedorConsulta = this.torcedorConsulta;
		this.torcedorCampos.telefoneCampos = this.telefoneCampos;
		this.torcedorTelefonesCols.telefoneCols = this.telefoneCols;
		this.torcedorTelefoneValidations.torcedorCampos = this.torcedorCampos;
		this.torcedorTelefoneValidations.telefoneCampos = this.telefoneCampos;
		new Configure().init();
		this.inicializado = true;
		this.injetarFila.forEach(o => this.injetar(o));
		this.authorization.init();
		this.senha.init();
		this.usuario.init();
		this.logado.init();
		this.cache.init();
		this.fcAxios.init();
		this.telefoneConsulta.init();
		this.torcedorConsulta.init();
		this.telefoneTipoConsulta.init();
		this.telefoneCampos.init();
		this.torcedorCols.init();
		this.telefoneCols.init();
		this.telefoneTipoCampos.init();
		this.telefoneTipoCols.init();
		this.controleSessao.init();
		this.torcedorCampos.init();
		this.torcedorTelefonesCols.init();
		this.torcedorTelefoneValidations.init();
		let onStart = new OnStart();
		onStart.torcedorPermissoes = this.torcedorPermissoes;
		onStart.exec();
	}

	injetar(o) {
		if (!this.inicializado) {
			this.injetarFila.add(o);
			return;
		}
		if (o instanceof App) {
			let x = o;
			x.logado = this.logado;
		} else if (o instanceof TorcedorFormConsulta) {
			let x = o;
			x.torcedorConsulta = this.torcedorConsulta;
			x.torcedorUtils = this.torcedorUtils;
			x.torcedorCampos = this.torcedorCampos;
			x.torcedorCols = this.torcedorCols;
		} else if (o instanceof FcBotao) {
			let x = o;
			x.tema = this.tema;
		} else if (o instanceof Paginacao) {
			let x = o;
			x.tema = this.tema;
			x.consultandoExcel = this.consultandoExcel;
		} else if (o instanceof TorcedorEdit) {
			let x = o;
			x.campos = this.torcedorCampos;
			x.torcedorTelefonesCols = this.torcedorTelefonesCols;
			x.permissoes = this.torcedorPermissoes;
		} else if (o instanceof TelefoneEdit) {
			let x = o;
			x.campos = this.telefoneCampos;
			x.permissoes = this.telefonePermissoes;
		} else if (o instanceof ModalCamposAlterados) {
			let x = o;
			x.camposAlterados = this.vCamposAlterados;
		} else if (o instanceof BotaoLogout) {
			let x = o;
			x.efetuarLogout = this.efetuarLogout;
		} else if (o instanceof LoginView) {
			let x = o;
			x.usuario = this.usuario;
			x.senha = this.senha;
			x.efetuarLogin = this.efetuarLogin;
		} else if (o instanceof TelefoneFormConsulta) {
			let x = o;
			x.telefoneConsulta = this.telefoneConsulta;
			x.telefoneUtils = this.telefoneUtils;
			x.telefoneCampos = this.telefoneCampos;
			x.telefoneCols = this.telefoneCols;
		} else if (o instanceof TelefoneTipoEdit) {
			let x = o;
			x.campos = this.telefoneTipoCampos;
			x.permissoes = this.permissoesDefault;
		} else if (o instanceof LoadingView) {
			let x = o;
			x.loadings = this.loadings;
			x.variaveisDeAmbiente = this.variaveisDeAmbiente;
		} else if (o instanceof MessageView) {
			let x = o;
			x.mensagens = this.mensagens;
			x.variaveisDeAmbiente = this.variaveisDeAmbiente;
		} else if (o instanceof SearchService) {
			let x = o;
			x.fcAxios = this.fcAxios;
		}
	}

	injetarFathers(o) {}

	injetarObservers(o) {
		if (o instanceof App) {
			let x = o;
			x.observar(x.logado);
		} else if (o instanceof TorcedorFormConsulta) {
			let x = o;
			x.observar(x.torcedorConsulta);
			x.observar(x.torcedorCampos);
		} else if (o instanceof FcBotao) {
			let x = o;
			x.observar(x.tema);
		} else if (o instanceof FormConsultaFiltros) {
			let x = o;
			x.observar(x.props.consulta);
			x.observar(x.filtro);
		} else if (o instanceof InputBind) {
			let x = o;
			x.observar(x.props.bind);
		} else if (o instanceof SelectBindSimple) {
			let x = o;
			x.observar(x.props.bind);
		} else if (o instanceof SelectMultipleBind) {
			let x = o;
			x.observar(x.props.bind);
		} else if (o instanceof PopoverDate) {
			let x = o;
			x.observar(x.props.bind);
		} else if (o instanceof Calendario) {
			let x = o;
			x.observar(x.mesAno);
			x.observar(x.props.bind);
		} else if (o instanceof Paginacao) {
			let x = o;
			x.observar(x.consultandoExcel);
			x.observar(x.props.consulta);
			x.observar(x.tema);
		} else if (o instanceof Tabela) {
			let x = o;
			x.observar(x.props.bind);
		} else if (o instanceof TorcedorEdit) {
			let x = o;
			x.observar(x.permissoes);
			x.observar(x.campos);
		} else if (o instanceof FormComponentErrorBinding) {
			let x = o;
			x.observar(x.props.bind);
		} else if (o instanceof TelefoneEdit) {
			let x = o;
			x.observar(x.campos);
		} else if (o instanceof SelectBind) {
			let x = o;
			x.observar(x.props.bind);
		} else if (o instanceof FormEditButtons) {
			let x = o;
			x.observar(x.props.campos);
		} else if (o instanceof PopoverErros) {
			let x = o;
			x.observar(x.props.campos);
		} else if (o instanceof TelefoneFormConsulta) {
			let x = o;
			x.observar(x.telefoneConsulta);
			x.observar(x.telefoneCampos);
		} else if (o instanceof TelefoneTipoEdit) {
			let x = o;
			x.observar(x.campos);
		} else if (o instanceof LoadingView) {
			let x = o;
			x.observar(x.loadings);
		} else if (o instanceof MessageView) {
			let x = o;
			x.observar(x.mensagens);
		}
	}

	static start() {
		Context.createCDI = () => new ReactContext();
		Context.getContext();
	}
}
