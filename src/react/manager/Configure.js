import Ambiente from '../../fc/config/Ambiente';
import AmbienteConfigWeb from '../../fc/config/AmbienteConfigWeb';
import CampoConsulta from '../../fc/components/consulta/CampoConsulta';
import CampoConsultaWebRenders from '../../fc/components/consulta/CampoConsultaWebRenders';
import Console from '../../app/misc/utils/Console';
import StringCompare from '../../commom/utils/string/StringCompare';
import {message} from 'antd';

export default class Configure {

	init() {

		Ambiente.set(new AmbienteConfigWeb());
		CampoConsulta.renders = new CampoConsultaWebRenders();

		Console.observers.add(o => {
			if (StringCompare.eq(o.type, "error")) {
				message.error(o.value);
			}
		});

	}

}
