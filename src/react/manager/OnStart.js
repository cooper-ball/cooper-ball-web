export default class OnStart {

	exec() {
		this.torcedorPermissoes.setIncluirTelefones(true);
		this.torcedorPermissoes.setExcluirTelefones(true);
	}

}
