import React from 'react';
import SuperComponent from '../app/misc/components/SuperComponent';

export default class Span extends SuperComponent {

	render0() {
		return (
			<span style={this.newStyle().join(this.props.style).get()} onClick={this.props.onPress} className={this.props.className} id={this.props.id}>{this.props.text}</span>
		);
	}
}

Span.defaultProps = SuperComponent.defaultProps;
