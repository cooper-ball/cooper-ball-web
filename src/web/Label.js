import React from 'react';
import SuperComponent from '../app/misc/components/SuperComponent';

export default class Label extends SuperComponent {

	render0() {
		return (
			<label
				style={this.newStyle().join(this.props.style).get()}
				onClick={this.props.onPress}
				className={this.props.className}
				id={this.props.id}
				htmlFor={this.props.htmlFor}
				title={this.props.title}
			>{this.props.text}</label>
		);
	}
}

Label.defaultProps = SuperComponent.defaultProps;
