import Null from '../../commom/utils/object/Null';
import StringEmpty from '../../commom/utils/string/StringEmpty';
import StringParse from '../../commom/utils/string/StringParse';
import UBoolean from '../../app/misc/utils/UBoolean';
import UData from '../../app/misc/utils/UData';
import UInteger from '../../app/misc/utils/UInteger';

export default class AbstractStorage {

	constructor(storage) {
		this.storage = storage;
	}
	set(key, value) {
		let s = StringParse.get(value);
		if (StringEmpty.is(s)) {
			this.remove(key);
		} else {
			this.storage.setItem(key, s);
		}
	}
	get(key) {
		return this.storage.getItem(key);
	}
	remove(key) {
		this.storage.removeItem(key);
	}
	clear() {
		this.storage.clear();
	}
	key(index) {
		return this.storage.key(index);
	}
	getData(key) {
		let s = this.get(key);
		if (StringEmpty.is(s)) {
			return null;
		} else {
			return UData.strToDate(s);
		}
	}
	setData(key, value) {
		if (Null.is(value)) {
			this.remove(key);
		} else {
			this.set(key, value.toJSON());
		}
	}
	getInt(key) {
		return UInteger.toInt(this.get(key));
	}
	getObject(key, classe) {
		return JSON.parse(this.get(key));
	}
	isTrue(key) {
		return UBoolean.isTrue(this.get(key));
	}
}
