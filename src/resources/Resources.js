import ArrayLst from '../commom/utils/array/ArrayLst';
import logo_png from './logo.png';
import user_png from './user.png';

export default class Resources {}
Resources.logo = logo_png;
Resources.user = user_png;
Resources.MAP =
		new Map()
		.set("logo", Resources.logo)
		.set("user", Resources.user)
		;
Resources.list = ArrayLst.build(
	Resources.logo
	, Resources.user
);
