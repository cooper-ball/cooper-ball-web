export default class IdParam {

	id = 0;

	constructor(id){
		this.id = id;
	}

	getId() {
		return this.id;
	}

	toJSON() {
		return "{\"id\": "+this.id+"}";
	}

}
