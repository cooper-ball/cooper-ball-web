import ArrayLst from '../commom/utils/array/ArrayLst';
import Cache from '../react/cache/Cache';
import Console from '../app/misc/utils/Console';
import Null from '../commom/utils/object/Null';
import Sessao from './Sessao';
import StringBox from '../commom/utils/string/StringBox';
import StringCompare from '../commom/utils/string/StringCompare';
import StringParse from '../commom/utils/string/StringParse';
import UCommons from '../app/misc/utils/UCommons';

export default class Servico {

	static creator;
	static impl;
	static itensRodando = new Map();
	static executarQuandoVazio = new ArrayLst();
	static fila = new ArrayLst();

	static addExecutarQuandoVazio(func) {
		Servico.executarQuandoVazio.add(func);
		Servico.runExecutarQuandoVazio();
	}

	addInterceptor(interceptor) {
		Servico.getImpl().addInterceptor(interceptor);
	}

	static getImpl() {
		if (Null.is(Servico.impl)) {
			if (Null.is(Servico.creator)) {
				throw new Error("Servico.creator === null");
			} else {
				Servico.impl = Servico.creator();
			}
		}
		return Servico.impl;
	}

	static debug = true;
	static rodando = 0;
	static esperando = 0;
	static keysCount = 0;
	static exception;

	post(postParams) {
		this.checkInstance();
		postParams.key = postParams.uri + " " + StringParse.get(postParams.params);
		if (!Servico.fila.exists(o => StringCompare.eq(postParams.key, o.key))) {
			Servico.fila.add(postParams);
			this.runNext(null);
		}
	}

	runNext(anterior) {
		Servico.fila.removeIf(o => StringCompare.eq(anterior, o.key));
		setTimeout(() => this.run());
	}

	run() {

		if (Servico.rodando > 0 || Servico.fila.isEmpty()) {
			return;
		}

		let postParams = Servico.fila.removeFirst();

		let value = postParams.uri + " " + StringParse.get(postParams.params);

		Servico.fila.removeIf(o => StringCompare.eq(value, o.key));

		if (Servico.rodando > 0) {
			Servico.esperando++;
			setTimeout(() => {
				Servico.esperando--;
				this.post(postParams);
			}, 500);
			Console.log("Servico", "esperado post ...");
			return;
		}

		let key = Servico.nextPost(value);

		let sparams = StringParse.get(postParams.params);

		let cache = Cache.get(postParams.uri, sparams, postParams.segundosCache);

		let onSuccess =
			res => {

				try {
					if (Null.is(cache)) {
						Cache.set(postParams.uri, sparams, res);
					}
					postParams.onSuccess(res);
				} catch (e) {
					Servico.exception = e;
					Console.error("Servico", "ocorreu um erro no callback do servico " + postParams.uri);
					Console.error("params", postParams.params);
					Console.error("exception", Servico.exception);
					this.callOnError(postParams, key);
				} finally {
					Servico.finish(key);
					this.runNext(value);
				}
			}
		;

		if (!Null.is(cache)) {
			onSuccess(cache);
			return;
		}

		let onError =
			res => {
				try {
					Servico.exception = res.body;
					if (Null.is(Servico.exception)) {
						Servico.exception = UCommons.cast(res);
					}
					Console.error("Servico", "ocorreu um erro no servico " + postParams.uri);
					Console.error("params", postParams.params);
					Console.error("exception", Servico.exception);
					this.callOnError(postParams, key);
				} finally {
					Servico.finish(key);
					this.runNext(value);
				}
			}
		;

		let onFinally =
			() => {
				try {
					if (!Null.is(postParams.onFinally)) {
						postParams.onFinally();
					}
				} finally {
					Servico.finish(key);
					this.runNext(value);
				}
			}
		;

		Servico.getImpl().post(postParams.uri, postParams.params, postParams.headers, onSuccess, onError, onFinally);
	}

	callOnError(postParams, key) {

		let message = Servico.exception.message;

		if (Null.is(postParams.onError)) {
			this.callOnErrorDefault(postParams, message);
		} else {
			try {
				postParams.onError(message);
			} catch (e2) {
				Console.error("exception", e2);
				this.callOnErrorDefault(postParams, message);
			}
		}

		Servico.finish(key);
	}

	callOnErrorDefault(postParams, message) {
		Servico.impl.onErrorDefault(postParams.uri, postParams.params, message);
	}

	static finish(key) {
		Servico.rodando--;
		Servico.itensRodando.delete(key);
		Servico.runExecutarQuandoVazio();
	}

	static runExecutarQuandoVazio() {
		if (Servico.esperando === 0 && !Servico.executarQuandoVazio.isEmpty()) {
			let list = Servico.executarQuandoVazio.concat(new ArrayLst());
			Servico.executarQuandoVazio.clear();
			list.forEach(f => f());
		}
	}

	static nextPost(value) {
		Servico.keysCount++;
		Servico.itensRodando.set(Servico.keysCount, value);
		Servico.rodando++;
		return Servico.keysCount;
	}

	static esperar() {
		let count = 0;
		while (Servico.rodando > 0) {
			count++;
			if (count > 20 && !Servico.debug) {
				let s = new StringBox("Travou (1)");
				Servico.itensRodando.forEach((k, v) => s.add("; " + k + " - " + v));
				throw new Error(s.get());
			}
		}
	}

	static newInstance = () => new Servico();

	static getInstance() {
		return Sessao.getInstance("Servico", Servico.newInstance, o => {});
	}
	checkInstance() {
		Sessao.checkInstance("Servico", this);
	}
}
