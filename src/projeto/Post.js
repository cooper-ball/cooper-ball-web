import IdParam from './IdParam';
import IntegerIs from '../commom/utils/integer/IntegerIs';
import Servico from './Servico';
import UInteger from '../app/misc/utils/UInteger';

export default class Post {

	constructor(uri, params, onSuccess) {

		this.uri = uri;
		this.onSuccess = onSuccess;

		if (IntegerIs.is(params)) {
			this.params = new IdParam(UInteger.toInt(params));
		} else {
			this.params = params;
		}

	}

	onError = Post.onErrorDefault;
	segundosCache = 600;

	static onErrorDefault;

	run() {
		Servico.getInstance().post(this);
	}

	setHeaders(o){
		this.headers = o;
		return this;
	}

	setOnError(o){
		this.onError = o;
		return this;
	}

	setOnFinally(o){
		this.onFinally = o;
		return this;
	}

	setSegundosCache(value) {
		this.segundosCache = value;
		return this;
	}

}
